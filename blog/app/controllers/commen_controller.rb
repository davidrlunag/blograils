class CommenController < ApplicationController
  before_action :set_comman, only: [:show, :edit, :update, :destroy]

  # GET /commen
  # GET /commen.json
  def index
    @commen = Comman.all
  end

  # GET /commen/1
  # GET /commen/1.json
  def show
  end

  # GET /commen/new
  def new
    @comman = Comman.new
  end

  # GET /commen/1/edit
  def edit
  end

  # POST /commen
  # POST /commen.json
  def create
    @comman = Comman.new(comman_params)

    respond_to do |format|
      if @comman.save
        format.html { redirect_to @comman, notice: 'Comman was successfully created.' }
        format.json { render action: 'show', status: :created, location: @comman }
      else
        format.html { render action: 'new' }
        format.json { render json: @comman.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /commen/1
  # PATCH/PUT /commen/1.json
  def update
    respond_to do |format|
      if @comman.update(comman_params)
        format.html { redirect_to @comman, notice: 'Comman was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @comman.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /commen/1
  # DELETE /commen/1.json
  def destroy
    @comman.destroy
    respond_to do |format|
      format.html { redirect_to commen_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comman
      @comman = Comman.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comman_params
      params.require(:comman).permit(:post_id, :\, :body)
    end
end
