require 'test_helper'

class CommenControllerTest < ActionController::TestCase
  setup do
    @comman = commen(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:commen)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create comman" do
    assert_difference('Comman.count') do
      post :create, comman: { \: @comman.\, body: @comman.body, post_id: @comman.post_id }
    end

    assert_redirected_to comman_path(assigns(:comman))
  end

  test "should show comman" do
    get :show, id: @comman
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @comman
    assert_response :success
  end

  test "should update comman" do
    patch :update, id: @comman, comman: { \: @comman.\, body: @comman.body, post_id: @comman.post_id }
    assert_redirected_to comman_path(assigns(:comman))
  end

  test "should destroy comman" do
    assert_difference('Comman.count', -1) do
      delete :destroy, id: @comman
    end

    assert_redirected_to commen_path
  end
end
